#!/bin/bash
userID=$(id -u)
sudo chmod 644 -R .
sudo chown $userID:$userID -R .
sudo find ./ -type d -exec sudo chmod 755 {} +